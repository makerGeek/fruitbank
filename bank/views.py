from django.http import HttpResponse, HttpRequest
from django.shortcuts import render
from sklearn import tree
# Create your views here.
from django.views import View
from django.conf import settings
import os
import pickle
from .models import Fruits

class BankView(View):
    def get(self, request: HttpRequest):
        return render(request, 'bank.html',)

    def post(self, request):
        weight = request.POST.get('weight')
        size = request.POST.get('size')
        # features = [[140, 1], [130, 1], [150, 0], [170, 0]]
        # labels = [0, 0, 1, 1]
        # clf = tree.DecisionTreeClassifier()
        # clf = clf.fit(features, labels)

        with open(os.path.join(settings.BASE_DIR,'classifier'), 'rb') as f:
            clf = pickle.load(f)
        result = clf.predict([[weight, size]])
        fruit = Fruits(weight=weight, size=size, family=result)
        fruit.save()
        return render(request, 'bank.html', {'result':result})