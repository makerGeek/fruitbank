from django.db import models

# Create your models here.

class Fruits(models.Model):
    weight = models.FloatField()
    size = models.FloatField()
    family = models.IntegerField()
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "family" + str(self.family)